import { Component, Vue, Prop, Emit } from 'vue-property-decorator';
import WithRender from '../templates/filters.html';
import { Cars } from '../interfaces/Cars';
import { State, Action } from 'vuex-class'

@WithRender
@Component
export default class Filters extends Vue {
    @State('cars') stateCars: Cars[];

    public results: Cars[];
    public selectedTypes: string[] = [];

    public options: string[] = ["Price (Ascending)", "Price (Descending)", "Make"];
    public types: string[] = ["Opel", "Nissan", "Citroën", "Kia", "Renault", "Peugeot", "Ford", "Volkswagen", "Toyota", "BMW", "VW", "Audi", "Mini"];

    mounted() {
        this.results = this.stateCars;
    }

    // Function for sorting the list on filter click
    filterResult(option: string) {
        if (this.results.length == 0) {
            this.results = this.stateCars;
        }

        if(option === "Price (Ascending)") {
            this.results = this.results.sort((a,b) => (a.pricing.price > b.pricing.price) ? 1 : ((b.pricing.price > a.pricing.price) ? -1 : 0));
        }

        if(option === "Price (Descending)") {
            this.results = this.results.sort((a,b) => (a.pricing.price < b.pricing.price) ? 1 : ((b.pricing.price < a.pricing.price) ? -1 : 0));
        }

        // Couldn't get around the identifier expected error
        // @Emit() public interface() => { return this.cars };
        this.$emit('interface', this.results);
    }

    // Function to filter data based on selected make checkboxes
    typeClicked(type: string) {
        if(this.selectedTypes.indexOf(type) < 0) {
            this.selectedTypes.push(type);
        }
        else {
            this.selectedTypes.splice(this.selectedTypes.indexOf(type),1);
        }
        
        if(this.selectedTypes.length == 0) {
            this.results = this.stateCars;
        }
        else {
            let self = this;
            this.results = this.stateCars.filter((value) =>{
                return self.selectedTypes.indexOf(value.car.make) >= 0;
            });
        }
        
        // Relay the result back to parent Car List component
        this.$emit('interface', this.results);
    }
}