// Define Car model

export interface Cars {
    id: string,
    available: boolean,
    segment: string,
    detailUrl: string,
    isAvailableAtShortNotice: boolean,
    car: {
        make: string,
        model: string,
        version: string,
        kw: number,
        ps: number,
        gearingType: string,
        fueltype: string
    },
    teaser: {
        title: string,
        subtitle: string,
        ribbon: string,
        teaserImage: string,
        equipmentHighlights: string[]
    },
    labels: string[],
    pricing: {
        price: number,
        currencyIsoCode: string,
        currencySymbol: string
    }
}