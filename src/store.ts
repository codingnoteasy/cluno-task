import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cars: [],
    // Can be moved to environment variables in proper production setup
    baseUrl: "https://api.staging.cluno.com/offerservice/v1/offer/"
  },
  mutations: {
    // Store cars value in State Variabe to reduce API calls
    cars(state, cars) {
      state.cars = cars;
    },
  },
  actions: {
    // REST get call to get list f cars
    fetchCars({ commit })  {    
      return new Promise((resolve, reject) => {
        axios.get(this.state.baseUrl + "query")
        .then((response) => {
          commit("cars", response.data.items);
          resolve(response);
        })
        .catch((error => {
          reject(error);
        }));
      });
    },

    // REST get call to get a car details
    fetchCarDetails({}, car) {
      return axios.get(this.state.baseUrl + car.id);
    }
  }
})
