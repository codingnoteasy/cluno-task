import { Component, Vue } from 'vue-property-decorator';
import WithRender from '../templates/car-details.html?style=../styles/car-details.scss';
import { Action } from 'vuex-class'

@WithRender
@Component
export default class CarDetails extends Vue {
    @Action('fetchCarDetails') actionFetchCarDetails: Function;
    public car: any;
    public isLoaded: boolean = false;

    // Used to make an API call via store to get car details
    mounted () {
        let carId = this.$route.params.id;
        let self = this;
        this.actionFetchCarDetails({ id: carId }).then((response: any) => {
            self.car = response.data;
            self.isLoaded = true;
        })
        .catch((error: any) => {
            console.log(error)
        });
    }
}