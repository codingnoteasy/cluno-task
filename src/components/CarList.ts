import { Component, Vue } from 'vue-property-decorator';
import WithRender from '../templates/car-list.html?style=../styles/car-list.scss';
import { State, Action } from 'vuex-class'
import { Cars } from '../interfaces/Cars';
import Filters from '../components/Filters';

@WithRender
@Component({
    components: {
      Filters,
    },
})
export default class CarList extends Vue {
    @State('cars') stateCars: Cars[];
    @Action('fetchCars') actionFetchCars: Function;

    public carList: Cars[] = [];

    get cars() {
        return this.carList;
    };

    // Life cycle hook used to load the Car List data
    mounted () {
        if(this.stateCars.length == 0) {
            this.actionFetchCars().then((response: any) => {
                this.carList = response.data.items;
            })
            .catch((error: any) => {
                console.log(error);
            });
        }
        else {
            this.carList = this.stateCars;
        }
    }

    seeDetails(car: Cars) {
        this.$router.push({name:'car-details', params:{ id:car.id }});
    }

    // Catch any change due to filter component
    filteredResults(cars: Cars[]) {
        this.carList = cars;
    }
}