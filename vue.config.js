module.exports = {
    configureWebpack: {
        module: {
        rules: [
            {
                test: /.html$/,
                loader: "vue-template-loader",
                exclude: /index.html/
            },
            {
                test: /\.scss$/,
                use: ['postcss-loader', 'sass-loader']
            },
            {
                enforce: 'post',
                test: /\.scss$/,
                use: ['style-loader']
            }
        ]
        }
    }
}