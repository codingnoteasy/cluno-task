# cluno

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Plugins Used
```
"axios": To make REST calls
```
```
"bootstrap-vue": Basic layout of page and can provide many beautiful and responsive features like dropdowns, carousel etc
```
```
"node-sass", "sass-loader", "style-loader": Ability to use scss thus post process compilation of scss to css
```
```
"vue-property-decorator" : To help write class style code the typescript way in Vue
```
```
"vue-router": Able to have multiple routes in single page applications
```
```
"vuex", "vuex-class": Able to use State and State Mangement for the data and connections of client and server side as this enables data sharing an reduce redundant API calls 
```
```
"vue-template-loader": To be able to make non single page component architecture as I am still not convinced about its advantage when it comes to building large scale applications. More psedo microservice type approach of breaking into smaller components seems like more easier to maintain but then again its one own choice and I can work either ways
```